# frozen_string_literal: true

ActiveSupport::Dependencies.autoload_paths.delete(Rails.root.join("app", "services").to_s)
