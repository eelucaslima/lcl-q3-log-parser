# frozen_string_literal: true

require "sidekiq/web"

Rails.application.routes.draw do
  root to: "games#index"

  mount Sidekiq::Web, at: "/sidekiq"

  resources :games, only: %i[new create show]
end
