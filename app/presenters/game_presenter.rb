# frozen_string_literal: true

class GamePresenter
  def initialize(game)
    @game = game
  end

  def as_json
    hash = info.merge(grouped_by_death_cause)

    JSON.pretty_generate(hash)
  end

  private

  def info
    {
      "Task-1": {
        "game_#{@game.id}": {
          "total_kills": @game.deaths.count,
          "players": @game.players.pluck(:name),
          "kills": @game.players.pluck(:name, :score).to_h
        }
      }
    }
  end

  def grouped_by_death_cause
    {
      "Task-2": {
        "game-#{@game.id}": {
          "kills_by_means": @game.deaths.select(:meaning).group(:meaning).count
        }
      }
    }
  end
end
