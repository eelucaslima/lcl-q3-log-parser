# frozen_string_literal: true

class Death < ApplicationRecord
  belongs_to :game
  belongs_to :killed, class_name: "Player", inverse_of: "deaths"
  belongs_to :killer, class_name: "Player", inverse_of: "kills", optional: true

  validates :game_id, presence: true
  validates :killed_id, presence: true

  after_create :update_players_score

  private

  def update_players_score
    return removes_a_score_from_player if killer.nil? || killer == killed

    add_a_score_to_player
  end

  def removes_a_score_from_player
    killed.score -= 1

    killed.save
  end

  def add_a_score_to_player
    killer.score += 1

    killer.save
  end
end
