# frozen_string_literal: true

class Player < ApplicationRecord
  belongs_to :game

  has_many :deaths, inverse_of: "killed", foreign_key: "killed_id"
  has_many :kills, class_name: "Death", inverse_of: "killer", foreign_key: "killer_id"

  before_validation :score_is_zero_for_the_new_player, on: :create

  validates :game_id, presence: true
  validates :score, presence: true
  validates :name, uniqueness: { scope: :game_id }

  validate :name_cannot_be_world

  private

  def score_is_zero_for_the_new_player
    self.score = 0
  end

  def name_cannot_be_world
    errors.add(:name, "cannot be <world>") if name.present? && name == "<world>"
  end
end
