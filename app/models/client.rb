# frozen_string_literal: true

class Client
  def initialize(game:, number:, name: nil)
    @game = game
    @number = number
    @name = name
    @player = find_player
  end

  def update
    return change_player_name if @player.present? && @player.client_number.present?
    return reconnect_player if @player.present?

    create_player
  end

  def disconnect
    @player.client_number = nil

    @player.save
  end

  private

  def find_player
    return find_player_by_client if find_player_by_client.present?

    find_player_by_name
  end

  def find_player_by_client
    @game.players.find_by_client_number(@number)
  end

  def find_player_by_name
    @game.players.find_by_name(@name)
  end

  def change_player_name
    @player.name = @name

    @player.save
  end

  def reconnect_player
    @player.client_number = @number

    @player.save
  end

  def create_player
    @game.players.create(name: @name, client_number: @number)
  end
end
