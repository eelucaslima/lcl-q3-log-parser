# frozen_string_literal: true

class GamesController < ApplicationController
  def create
    log_path = "spec/fixtures/files/qgames.log"

    LogParserJob.perform_later(log_path: "spec/fixtures/files/qgames.log") if File.exist?(log_path)

    redirect_to root_path
  end

  def index
    @games = Game.all
  end

  def show
    @game = Game.find_by_id(params[:id])

    if @game
      @game_presenter = GamePresenter.new(@game).as_json

      render json: @game_presenter
    else
      render json: {}
    end
  end
end
