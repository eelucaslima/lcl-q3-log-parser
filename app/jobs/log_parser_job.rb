# frozen_string_literal: true

class LogParserJob < ApplicationJob
  queue_as :default

  def perform(log_path:)
    File.foreach(log_path) do |line|
      @parsed_line = Services::LineParser.new(line).call

      game_change_factory
    end
  end

  private

  def game_change_factory
    case @parsed_line[:action]
    when "InitGame"
      @game = Game.create
    when "ClientUserinfoChanged"
      change_client_info
    when "Kill"
      save_death_record
    when "ClientDisconnect"
      disconnect_client
    end
  end

  def change_client_info
    client = Client.new(game: @game, number: @parsed_line[:client_number], name: @parsed_line[:name])

    client.update
  end

  def save_death_record
    killer = Player.find_by(game: @game, client_number: @parsed_line[:killer_number])

    killed = Player.find_by(game: @game, client_number: @parsed_line[:killed_number])

    meaning = MeanOfDeath.meaning(@parsed_line[:cod_meaning])

    @game.deaths.create(killer: killer, killed: killed, meaning: meaning)
  end

  def disconnect_client
    client = Client.new(game: @game, number: @parsed_line[:client_number])

    client.disconnect
  end
end
