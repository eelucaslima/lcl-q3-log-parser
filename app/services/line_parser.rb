# frozen_string_literal: true

module Services
  class LineParser
    def initialize(line)
      @line = line
      @data = []
    end

    def call
      action, *@data = @line[7..-1].split(": ")

      parsed_data = data_parser(action)

      { action: action }.merge(parsed_data)
    end

    private

    def data_parser(action)
      case action
      when "ClientUserinfoChanged"
        client_number_and_player_name
      when "Kill"
        kill_info
      when "ClientDisconnect"
        client_number
      else
        {}
      end
    end

    def client_number_and_player_name
      client_number = @data.first.split.first.to_i

      name = @data.first.split(" n\\").second.split("\\t").first

      { client_number: client_number.to_i, name: name }
    end

    def kill_info
      killer_number, killed_number, cod_meaning = @data.first.split

      { killer_number: killer_number.to_i, killed_number: killed_number.to_i, cod_meaning: cod_meaning.to_i }
    end

    def client_number
      { client_number: @data.first.to_i }
    end
  end
end
