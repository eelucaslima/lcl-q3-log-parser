[![Codeship Status for lcl-dev/cloudwalk/lcl-q3-log-parser](https://app.codeship.com/projects/ce6a8740-ced6-0138-9dc5-1e03d27ee81f/status?branch=master)](https://app.codeship.com/projects/407797)

# Steps to run the application

1. Build the image and run the containers (It takes a long time)

    `$ docker-compose up`

2. Create database (After containers start)
    
    `$ docker-compose exec web rails db:create`

3. Load database schema (After containers start)

    `$ docker-compose exec web rails db:schema:load`

4. Run tests (After containers start)

    `$ docker-compose exec -e RAILS_ENV=test web rspec`

# Browser

1. Go to http://localhost:3000
2. Click on `Import log` button
3. Click on `Carregar log` button
4. Wait until Sidekiq completes the task
5. Refresh http://localhost:3000