class CreatePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :players do |t|
      t.integer :game_id
      t.string :name
      t.integer :score
      t.integer :client_number

      t.timestamps
    end
    add_index :players, :game_id
  end
end
