class CreateDeaths < ActiveRecord::Migration[6.0]
  def change
    create_table :deaths do |t|
      t.integer :game_id
      t.integer :killed_id
      t.integer :killer_id
      t.string :meaning

      t.timestamps
    end
    add_index :deaths, :game_id
  end
end
