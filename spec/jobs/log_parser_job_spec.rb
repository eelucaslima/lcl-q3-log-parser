# frozen_string_literal: true

require "rails_helper"

RSpec.describe LogParserJob, type: :job do
  context "with one_game.log" do
    before do
      file = file_fixture("one_game.log")

      described_class.perform_now(log_path: file)
    end

    it "save only one game" do
      expect(Game.count).to eq 1
    end

    it "creates 4 players" do
      game = Game.first

      expect(game.players.count).to eq 4
    end

    it "creates 14 deaths" do
      game = Game.first

      expect(game.deaths.count).to eq 14
    end
  end

  context "with another_game.log" do
    before do
      file = file_fixture("another_game.log")

      described_class.perform_now(log_path: file)
    end

    it "creates player Dono da Bola with score eq to 5" do
      game = Game.first

      player = game.players.find_by(name: "Dono da Bola")

      expect(player.score).to eq 5
    end
  end
end
