# frozen_string_literal: true

require "rails_helper"

RSpec.describe Services::LineParser do
  it "parses InitGame line" do
    line =
      "  0:00 InitGame: \\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0"

    expected_parsed_line = {
      action: "InitGame"
    }

    parsed_line = described_class.new(line).call

    expect(parsed_line).to eq expected_parsed_line
  end

  it "parses ClientUserinfoChanged line" do
    line =
      "  0:07 ClientUserinfoChanged: 2 n\\Dono da Bola\\t\\1\\model\\sarge\\hmodel\\sarge"

    expected_parsed_line = {
      action: "ClientUserinfoChanged",
      client_number: 2,
      name: "Dono da Bola"
    }

    parsed_line = described_class.new(line).call

    expect(parsed_line).to eq expected_parsed_line
  end

  it "parses Kill line" do
    line =
      "  0:27 Kill: 1022 2 19: <world> killed Dono da Bola by MOD_FALLING"

    expected_parsed_line = {
      action: "Kill",
      killer_number: 1022,
      killed_number: 2,
      cod_meaning: 19
    }

    parsed_line = described_class.new(line).call

    expect(parsed_line).to eq expected_parsed_line
  end

  it "parses ClientDisconnect line" do
    line =
      " 21:10 ClientDisconnect: 2"

    expected_parsed_line = {
      action: "ClientDisconnect",
      client_number: 2
    }

    parsed_line = described_class.new(line).call

    expect(parsed_line).to eq expected_parsed_line
  end
end
