# frozen_string_literal: true

FactoryBot.define do
  factory :player do
    game { create(:game) }
    name { Faker::FunnyName.two_word_name }
    score { 0 }
    client_number { 2 }

    trait :first_player do
      name { "Player 1" }
    end

    trait :second_player do
      name { "Player 2" }
      client_number { 3 }
    end

    trait :another_player do
      client_number { game.players.last.client_number + 1 }
    end
  end
end
