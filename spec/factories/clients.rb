# frozen_string_literal: true

FactoryBot.define do
  factory :client do
    game { create(:game) }
    number { 2 }
    name { Faker::FunnyName.two_word_name }
    player { create(:player, client_number: number) }
  end
end
