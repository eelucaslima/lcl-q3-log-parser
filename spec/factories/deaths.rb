# frozen_string_literal: true

FactoryBot.define do
  factory :death do
    game { create(:game) }
    killed { create(:player, game: game) }
    killer { create(:player, :another_player, game: game) }
    meaning { "MOD_UNKNOWN" }

    trait :world_killer do
      killer { nil }
    end
  end
end
