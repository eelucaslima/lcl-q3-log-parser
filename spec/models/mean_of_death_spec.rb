# frozen_string_literal: true

require "rails_helper"

RSpec.describe MeanOfDeath, type: :model do
  describe ".meaning" do
    it "is MOD_ROCKET with index 6" do
      index = 6

      meaning = described_class.meaning(index)

      expect(meaning).to eq "MOD_ROCKET"
    end

    it "is MOD_RAILGUN with index 10" do
      index = 10

      meaning = described_class.meaning(index)

      expect(meaning).to eq "MOD_RAILGUN"
    end

    it "is MOD_TRIGGER_HURT with index 22" do
      index = 22

      meaning = described_class.meaning(index)

      expect(meaning).to eq "MOD_TRIGGER_HURT"
    end
  end
end
