# frozen_string_literal: true

require "rails_helper"

RSpec.describe Death, type: :model do
  let(:death) { build(:death) }

  it { expect(death).to be_valid }

  it "requires game_id" do
    death.game_id = nil

    death.valid?

    expect(death.errors[:game_id].count).to eq 1
  end

  it "requires killed_id" do
    death.killed_id = nil

    death.valid?

    expect(death.errors[:killed_id].count).to eq 1
  end

  it "doesn't require a killer" do
    death.killer = nil

    death.valid?

    expect(death.errors[:killer_id].count).to eq 0
  end
end
