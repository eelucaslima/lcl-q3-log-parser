# frozen_string_literal: true

require "rails_helper"

RSpec.describe Player, type: :model do
  context "when after builded" do
    let(:player) { build(:player, :first_player) }

    it "is valid" do
      expect(player.valid?).to eq true
    end

    it "is invalid with name eq world" do
      player.name = "<world>"

      expect(player.invalid?).to eq true
    end

    context "and when triggers validations" do
      it "score is zero for the new player" do
        player.score = 100

        player.valid?

        expect(player.score).to eq 0
      end

      it "requires game_id" do
        player.game_id = nil

        player.valid?

        expect(player.errors[:game_id].count).to eq 1
      end
    end
  end

  context "when after created" do
    let(:player) { create(:player) }

    it "requires score" do
      player.score = nil

      player.valid?

      expect(player.errors[:score].count).to eq 1
    end

    context "when triggers validations" do
      it "doesn't change the score player" do
        player.score = 15

        expect { player.valid? }.not_to(change { player.score })
      end
    end

    context "with another player of the same game" do
      describe "#name" do
        it "is unique" do
          another_player = build(:player, game: player.game, name: player.name)

          another_player.valid?

          expect(another_player.errors[:name].count).to eq 1
        end
      end
    end

    context "with another player of different game" do
      describe "#name" do
        it "isn't unique" do
          another_player = build(:player, name: player.name)

          another_player.valid?

          expect(another_player.errors[:name].count).to eq 0
        end
      end
    end
  end
end
