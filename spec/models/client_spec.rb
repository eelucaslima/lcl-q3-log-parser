# frozen_string_literal: true

require "rails_helper"

RSpec.describe Client, type: :model do
  let(:game) { create(:game) }

  context "without player" do
    let(:client) { described_class.new(game: game, name: "Player", number: 2) }

    describe "#update" do
      it "creates a player" do
        expect { client.update }.to(change { Player.count })
      end
    end
  end

  context "when player doesn't have a client number" do
    let(:player) { create(:player, game: game, name: "Player", client_number: nil) }

    context "when client is 2" do
      let(:client) { described_class.new(game: game, number: 2, name: player.name) }

      describe "#update" do
        it "sets player-client-number eq to 2" do
          client.update

          player.reload

          expect(player.client_number).to eq 2
        end
      end
    end
  end

  context "when player has a client number" do
    let(:player) { create(:player, game: game, name: "Player", client_number: 2) }
    let(:client) { described_class.new(game: game, number: player.client_number, name: "Player-1") }

    describe "#update" do
      it "sets player-name to eq client-name" do
        client.update

        player.reload

        expect(player.name).to eq "Player-1"
      end
    end

    describe "#disconnect" do
      it "sets player-client-number to nil" do
        client.disconnect

        player.reload

        expect(player.client_number).to be_nil
      end
    end
  end
end
