FROM ruby:2.5.1-alpine as develop-stage

RUN apk add --update --no-cache openssl \
                                tar \
                                build-base \
                                tzdata \
                                postgresql-dev

RUN wget https://yarnpkg.com/latest.tar.gz \
  && mkdir -p /opt/yarn \
  && tar -xf latest.tar.gz -C /opt/yarn --strip 1

ENV PATH=${PATH}:/opt/yarn/bin

RUN apk add nodejs-current

ARG UID
ARG USER
ENV HOST_USER_UID=$UID HOST_USER_NAME=$USER

RUN addgroup -S $HOST_USER_NAME && \
        adduser \
        -h "/home/docker_$HOST_USER_NAME" \
        -D \
        -G "$HOST_USER_NAME" \
        -u "$HOST_USER_UID" \
        "$HOST_USER_NAME"

ARG app_folder="/lcl-cloudwalk"
RUN mkdir -p ${app_folder}/node_modules && chown $HOST_USER_NAME:$HOST_USER_NAME -R ${app_folder}/node_modules

USER $HOST_USER_NAME

WORKDIR ${app_folder}

ENV RAILS_ENV=development
